import React from 'react'
import { View, Text, Image, Button, StyleSheet,Animated,Easing, TouchableOpacity,ImageBackground,AppRegistry,ActivityIndicator} from 'react-native'
import ImagePicker from 'react-native-image-picker'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import TopHomeImg from '../images/home_intro_bg.png'
// import backgr from '../images/backgr.jpg'
// import reactlogo from '../images/reactlogo.png'
import ImageResizer from 'react-native-image-resizer';
import ITlogo from '../images/home_itdlu_bg.png';
import borderLeftTop from '../images/top_left_angle.png';
import borderLeftBottom from '../images/bottom_left_angle.png';
import borderRightTop from '../images/top_right_angle.png';
import borderRightBottom from '../images/bottom_right_angle.png';

const resize = (uri) => {
  return new Promise((resolve, reject) => {
    ImageResizer.createResizedImage(uri, 240, 240, 'JPEG', 90, 0).then((response) => {
      resolve(response);
    }).catch((err) => {
      reject(err);
    });
  });
}
const createFormData = async (photo) => {
  if (photo != null) {
    
    await resize(photo.uri).then((response)=> {
      photo.uri = response.uri;
    }, error => {
      // Handle error
    });    
  }

  const data = new FormData();

  data.append("image", {
    name: photo.fileName,
    type: photo.type,
    uri:
      Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
  });

  return data;
};
export default class Home extends React.Component {
  state = {
    photo: null,
    process: false,
  }
  constructor () {
    super()
    // this.spinValue = new Animated.Value(0)
    // this.animatedValue1 = new Animated.Value(0)
    // this.animatedValue2 = new Animated.Value(0)
    // this.animatedValue3 = new Animated.Value(0)
  }
  componentDidMount () {
    // this.spin()
    // this.animate()
  }
  // spin () {
  //   this.spinValue.setValue(0)
  //   Animated.timing(
  //     this.spinValue,
  //     {
  //       toValue: 1,
  //       duration: 4000,
  //       easing: Easing.linear,
  //       useNativeDriver: true
  //     }
  //   ).start(() => this.spin())
  // }
  // animate () {
  //   this.animatedValue1.setValue(0)
  //   this.animatedValue2.setValue(0)
  //   this.animatedValue3.setValue(0)
    
  //   Animated.parallel([
  //     this.createAnimation(this.animatedValue1, 2000, Easing.ease),
  //     this.createAnimation(this.animatedValue2, 1000, Easing.ease, 1000),
  //     this.createAnimation(this.animatedValue3, 1000, Easing.ease, 2000),            
  //   ]).start()
  // }
  
  // createAnimation = (value, duration, easing, delay = 0) => {
  //    return Animated.timing(
  //      value,
  //      {
  //        toValue: 1,
  //        duration,
  //        easing,
  //        delay, 
  //        useNativeDriver: true,
  //      }
  //    )
  //  }
  handleChoosePhoto = () => {
    const options = {
      noData: true,
    }
    ImagePicker.launchImageLibrary(options, response => {
      if (response.uri) {
        this.setState({ photo: response })
      }
    })
  }

  handleUploadPhoto = async (image) => {
    this.setState({ process: true });
    let formdata = await createFormData(this.state.photo)
    const { navigate } = this.props.navigation;
    //formdata.append(this.state.photo);

    await fetch("http://pcvn.vn:8000/predict", {
      method: "POST",
      body: formdata
      // body: this.state.photo
    })
      .then(response =>        
        response.json()
      )
      .then(response => {
        console.log("upload succes", Object.values(response.probs));
        let pros=Object.values(response.probs);
        alert("Upload success!");
        this.setState({ photo: null, process: false });
        navigate('Details',{img:image, cate: response.category, pro: +Math.max(...pros)*100})
      })
      .catch(error => {
        alert("Upload failed!");
        this.setState({ process: false });
      });
  };
  takePhoto=()=>{
    const { navigate } = this.props.navigation;
    navigate('Camera');
  }
  render() {
    const { photo } = this.state
    const { navigate } = this.props.navigation;
    // const spin = this.spinValue.interpolate({
    //   inputRange: [0, 1],
    //   outputRange: ['0deg', '360deg'],
    // })
    // const scaleText = this.animatedValue1.interpolate({
    //   inputRange: [0, 1],
    //   outputRange: [0.5, 1.5],
    // })
    // const spinText = this.animatedValue2.interpolate({
    //   inputRange: [0, 1],
    //   outputRange: ['0deg', '720deg'],
    // })
    // const introButton = this.animatedValue3.interpolate({
    //   inputRange: [0, 1],
    //   outputRange: [-100, 500],
    // })
   
    if(this.state.process==true){
      return(
        <View style={[styles.container, styles.horizontal]}>
          <ActivityIndicator size={100} color="#0000ff" />
        </View>
      )
    }
    else{
      // return (
      //   <ImageBackground source={backgr} style={{width: '100%', height: '100%'}}>
      //   <View style={styles.container}>        
      //     {photo && (
      //       <React.Fragment>
      //         <Text style={styles.title2}>HÌNH ẢNH ĐÃ CHỌN</Text>
      //         <Image
      //           source={{ uri: photo.uri }}
      //           style={styles.preImage}
      //         />
      //         <View style={styles.upbutton}><Button title="Upload" style={styles.upbutton} onPress={()=>this.handleUploadPhoto(photo.uri)} /></View>     
      //       </React.Fragment>
      //     )}
      //     {photo ==null &&(
      //       <View style={{justifyContent: 'center', alignItems: 'center',}}>   
      //         <Animated.Image
      //           style={{
      //           width: 90,
      //           height: 80,  
      //           transform: [{rotate: spin}] }}
      //           source={reactlogo}
      //         />  
      //         <Animated.View  style={{ transform: [{scale: scaleText}] }}>
      //           <Text style={styles.title}>ỨNG DỤNG NHẬN DIỆN LOÀI HOA</Text> 
      //         </Animated.View>      
      //         <Animated.View
      //           style={{ marginTop: 20, transform: [{rotate: spinText}] }}>
      //           <Image
      //           source={LogoCo}
      //           style={styles.preImage}
      //           />
      //         </Animated.View>                  
      //       </View>
      //     )}
      //     <View style={styles.upbutton}><Button title="Take Photo" onPress={this.takePhoto} /></View> 
      //     <View style={styles.upbutton}><Button title="Choose Photo" onPress={this.handleChoosePhoto} /></View> 
      //     {/* <TouchableOpacity style={styles.upbutton} onPress={this.handleChoosePhoto}><Text>aa</Text></TouchableOpacity> */}
                      
      //   </View>
      //   </ImageBackground> 
      // )
      
      if(photo==null){
        return(
          <View style={styles.container}>
            <Image source={TopHomeImg} style={styles.TopImg}/>
            <Text style={styles.titleLogo}>AIFR DALAT</Text>
            <Image source={ITlogo} style={styles.ITLogoImg}/>
            <Text style={styles.titleIT}>ITDLU</Text>
            <Image source={borderLeftTop} style={styles.borTopLeft}/>
            <Image source={borderRightBottom} style={styles.borBottomRight}/> 
            <Text style={styles.mainTitle}>Để nhận diện chính xác, vui lòng  đặt hình ảnh đối tượng vào giữa</Text>
            <TouchableOpacity
              activeOpacity={0.5}
              onPress={this.takePhoto}
              style={styles.takePhotobtn}>
                <Text style={styles.takePhotoText}>Chụp hình</Text>
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.5}
              onPress={this.handleChoosePhoto}
              style={styles.choosePhotobtn}>
                <Text style={styles.takePhotoText}>Chọn hình</Text>
            </TouchableOpacity>
            {/* <View style={styles.upbutton}><Button title="Choose Photo" onPress={this.handleChoosePhoto} /></View>  */}
          </View>
        );
      }
      else{
        return(
          <View style={styles.container}>
            <Image source={TopHomeImg} style={styles.TopImg}/>
            <Text style={styles.titleLogo}>AIFR DALAT</Text>
            <Image source={ITlogo} style={styles.ITLogoImg}/>
            <Text style={styles.titleIT}>ITDLU</Text>
            <Text style={styles.mainTitle}>HÌNH ẢNH ĐÃ CHỌN</Text>
            <Image
              source={borderLeftTop}
              style={styles.imageleftTop}
            ></Image>
            <Image
              source={borderLeftBottom}
              style={styles.imageLeftbottom}
            ></Image>
            <Image
                 source={{ uri: photo.uri }}
                 style={styles.preImage}
            />
            <Image
              source={borderRightTop}
              style={styles.imageRightTop}
            ></Image>
            <Image
              source={borderRightBottom}
              style={styles.imageRightbottom}
            ></Image>
            <TouchableOpacity
              activeOpacity={0.5}
              onPress={this.takePhoto}
              style={styles.takePhotobtn}>
                <Text style={styles.takePhotoText}>Chụp hình</Text>
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.5}
              onPress={this.handleChoosePhoto}
              style={styles.choosePhotobtn}>
                <Text style={styles.takePhotoText}>Chọn hình</Text>
            </TouchableOpacity>
            <View>            
            <TouchableOpacity
              activeOpacity={0.5}
              onPress={this.handleUploadPhoto(photo.uri)}
              style={styles.uploadPhotobtn}>
                <Text style={styles.takePhotoText}>Upload</Text>
            </TouchableOpacity></View>
            {/* <View style={styles.upbutton}><Button title="Choose Photo" onPress={this.handleChoosePhoto} /></View>  */}
          </View>
        );
      } 
      
    }
  }
}
const styles= StyleSheet.create({
  preImage:{
    width: 300,
    height: 300,
    bottom: 45,
  },
  TopImg:{
    position: 'absolute',
    left:0,
    top:0
  },
  ITLogoImg:{
    position: 'absolute',
    width: 150,
    height: 60,
    right:4,
    top: 25,
  },
  titleIT: {
    position: 'absolute',
    width: 78,
    height: 38,
    left: 248,
    top: 44,

    fontFamily: 'Montserrat',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 28,
    lineHeight: 29,
    display: 'flex',
    alignItems: 'center',
    textAlign: 'center',

    color: '#6A8CAF',
  },
  container:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  upbutton:{
    marginTop:10,
    alignItems: "center",
    backgroundColor: "#DDDDDD",
    padding: 10
  },
  imageleftTop:{
    position: 'absolute',
    width: 76,
    height: 76,
    left: 20,
    top: 150,
  },
  imageLeftbottom:{
    position: 'absolute',
    width: 76,
    height: 76,
    left: 20,
    bottom: 240,
  },
  imageRightTop:{
    position: 'absolute',
    width: 76,
    height: 76,
    right: 20,
    top: 150,
  },
  imageRightbottom:{
    position: 'absolute',
    width: 76,
    height: 76,
    right: 20,
    bottom: 240,
  },
  titleLogo:{
    position: 'absolute',
    width: 164,
    height: 92,
    left: 23,
    top: 16,
    fontFamily: 'Montserrat',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 36,
    lineHeight: 44,
    display: 'flex',
    alignItems: 'center',
    textAlign: 'center',
    color: '#383838',
  },
  title:{
    textAlign:'center',
    marginTop:20,
    color:'yellow',
  },
  title2:{
    textAlign:'center',
    marginTop:20,
    fontSize:30,
    color:'yellow'
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
  borTopLeft:{
    position: 'absolute',
    width: 140,
    height: 140,
    left: 35,
    top: 175,
  },
  borBottomRight:{
    position: 'absolute',
    width: 140,
    height: 140,
    right:35,
    bottom:60,
  },
  mainTitle:{
    position: 'absolute',
    width: 250,
    height: 300,
    left: 60,
    top: 300,

    fontFamily: 'Montserrat',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 21,
    lineHeight: 30,
    display: 'flex',
    alignItems: 'center',
    textAlign: 'center',
    color: '#000000',
  },
  takePhotoText:{
    position: 'absolute',
    top:8,

    fontFamily: 'Montserrat',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 18,
    alignItems: 'center',
    textAlign: 'center',

    color: '#000000',
  },

  takePhotobtn:{
    position: 'absolute',
    width: 238,
    height: 47,
    left: 59,
    bottom: 150,
    alignItems: 'center',
    backgroundColor: '#75B79E',
    borderRadius: 32 ,
    shadowColor: "red",
    shadowOffset: {
      width: 4,
      height: 4,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,

    elevation: 5,
  },
  choosePhotobtn:{
    position: 'absolute',
    width: 238,
    height: 47,
    left: 59,
    bottom: 80,
    alignItems: 'center',
    backgroundColor: '#6A8CAF',
    borderRadius: 32 ,
    shadowColor: "red",
    shadowOffset: {
      width: 4,
      height: 4,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,

    elevation: 5,
  },
});
