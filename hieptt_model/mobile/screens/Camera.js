import React, {PureComponent} from 'react';
import {RNCamera} from 'react-native-camera';

import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {TouchableOpacity, Alert, StyleSheet, View, ActivityIndicator} from 'react-native';

import ImageResizer from 'react-native-image-resizer';
import { Button } from 'react-native-paper';


const resize = (uri) => {
  return new Promise((resolve, reject) => {
    ImageResizer.createResizedImage(uri, 240, 240, 'JPEG', 90, 0).then((response) => {
      resolve(response);
    }).catch((err) => {
      reject(err);
    });
  });
}
const createFormData = async (photo) => {
  if (photo != null) {
    
    await resize(photo.uri).then((response)=> {
      photo.uri = response.uri;
    }, error => {
      // Handle error
    });  
  }
  
  const data = new FormData();
  var str = photo.uri;
  var n = str.lastIndexOf('/');
  var result = str.substring(n + 1);
  data.append("image", {
    name: result,
    type: 'image/jpeg',
    uri:
      Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
  });
  return data;
};

export default class Camera extends PureComponent {
  constructor(props) {
    super(props);
    this.zoomRate = 0.005;
    this.state = {
      takingPic: false,
      flashMode: RNCamera.Constants.FlashMode.auto,
      // front -> true , back -> false
      cameraMode: true,
      //iconName off -> flash-off, auto -> flash-auto, on -> flash-on
      process:false
    };
  }

  takePicture = async () => {
    if (this.camera && !this.state.takingPic) {

      let options = {
        quality: 0.85,
        fixOrientation: true,
        forceUpOrientation: true,
      };

      this.setState({takingPic: true});

      try {
         const data = await this.camera.takePictureAsync(options);
         this.setState({photo: data});
         this.handleUploadPhotoCam(data.uri);
        //  Alert.alert('Success', JSON.stringify(data));
      } catch (err) {
        Alert.alert('Error', 'Failed to take picture: ' + (err.message || err));
        return;
      } finally {
        this.setState({takingPic: false});
      }
    }
  };

  
  onPressChangeCameraMode(){
      this.setState({ cameraMode: ! this.state.cameraMode });
  }

  handleUploadPhotoCam = async (image) => {
    this.setState({ process: true });
    let formdata = await createFormData(this.state.photo)
    const { navigate } = this.props.navigation;
    //formdata.append(this.state.photo);

    await fetch("http://pcvn.vn:8000/predict", {
      method: "POST",
      body: formdata
      // body: this.state.photo
    })
      .then(response =>        
        response.json()
      )
      .then(response => {
        let pros=Object.values(response.probs);
        alert("Upload success!");
        this.setState({ photo: null,process: false });
        navigate('Details',{img:image, cate: response.category, pro: +Math.max(...pros)*100})
      })
      .catch(error => {
        alert("Upload failed!");
        // navigate('Details',{img:image, otherParam: response})
      });
  };
  render() {
    if(this.state.process==true){
      return(
        <View style={[styles.container, styles.horizontal]}>
          <ActivityIndicator size={100} color="#0000ff" />
        </View>
      )
    }
    else{
      return (
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          captureAudio={false}
          style={{flex: 1}}
          type={ !(this.state.cameraMode) ? RNCamera.Constants.Type.front : RNCamera.Constants.Type.back }
          flashMode={this.state.flashMode}
          autoFocus={RNCamera.Constants.AutoFocus.on}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}>
          
          <TouchableOpacity
            activeOpacity={0.5}
            style={styles.btnChangeCam}
            onPress={this.onPressChangeCameraMode.bind(this)}>
            <Icon name="flash" size={50} color="#fff" />
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.5}
            style={styles.btnAlignment}
            onPress={this.takePicture}>
            <Icon name="camera" size={50} color="#fff" />
          </TouchableOpacity>
        </RNCamera>
      )
    }
  }
}

const styles = StyleSheet.create({
  btnAlignment: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginBottom: 20,
  },
  btnChangeCam: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
    marginRight: 20,
    marginTop: 20,
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
  container:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});