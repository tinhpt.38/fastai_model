import React from 'react'
import { StyleSheet, View, Text, Image } from "react-native";
import { ProgressBar, Colors } from 'react-native-paper';
import borderLeftTop from '../images/top_left_angle.png';
import borderLeftBottom from '../images/bottom_left_angle.png';
import borderRightTop from '../images/top_right_angle.png';
import borderRightBottom from '../images/bottom_right_angle.png';


export default class Detail extends React.Component {
  render() {
    const { navigate } = this.props.navigation;
    const { img,cate,pro } = this.props.route.params;
    console.log(pro);
    return (
      <View style={styles.container}>
      <Image
        source={borderLeftTop}
        style={styles.imageleftTop}
      ></Image>
      <Image
        source={borderLeftBottom}
        style={styles.imageLeftbottom}
      ></Image>
      <Image
        source={{uri:img}}
        resizeMode="stretch"
        style={styles.image}
      ></Image> 
       <Image
        source={borderRightTop}
        style={styles.imageRightTop}
      ></Image>
      <Image
        source={borderRightBottom}
        style={styles.imageRightbottom}
      ></Image>
      <Text style={styles.titleFlower}>Kết quả dự đoán:</Text>
      <Text style={styles.titleName}>{cate}</Text>  
      <ProgressBar style={styles.titlePro} progress={pro} color={Colors.green800} />
      <Text style={styles.proflower}>{pro.toFixed(3)}%</Text>
      {/* <Text style={styles.titleName}>Tên của loài hoa: {cate}</Text>      
      <View style={styles.group}>  
        <View style={styles.flowerRow}>
          <Text style={styles.flower}>Dự đoán:</Text>
          <View style={styles.title}>
            <ProgressBar style={styles.titlePro} progress={pro} color={Colors.green800} />
          </View>
        </View>
        <View style={styles.flowerRow}>
          <Text style={styles.proflower}>{pro.toFixed(3)}%</Text>
        </View>
      </View> */}
    </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(255,255,255,1)"
  },
  image: {
    position: 'absolute',
    width: 280,
    height: 280,
    left: 40,
    top: 50,
  },
  imageleftTop:{
    position: 'absolute',
    width: 76,
    height: 76,
    left: 27,
    top: 40,
  },
  imageLeftbottom:{
    position: 'absolute',
    width: 76,
    height: 76,
    left: 27,
    bottom: 300,
  },
  imageRightTop:{
    position: 'absolute',
    width: 76,
    height: 76,
    right: 27,
    top: 40,
  },
  imageRightbottom:{
    position: 'absolute',
    width: 76,
    height: 76,
    right: 30,
    bottom: 300,
  },
  group: {
    width: 303,
    height: 24,
    flexDirection: "row",
    marginTop: 30,
    marginLeft: 30
  },
  titleFlower: {
    top: 370,
    fontFamily: 'Montserrat',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 22,
    /* identical to box height */
    
    alignItems: 'center',
    textAlign: 'center',
    
    color: '#000000',
  },
  proflower: {
    fontFamily: "roboto-regular",
    textAlign:"center",
    fontSize: 20,
    top: 370,
    left:20,
  },
  titleName: {
    fontFamily: 'Montserrat',
    fontStyle: 'normal',
    fontWeight: 'bold',
    lineHeight: 20,
    /* identical to box height */

    display: 'flex',
    alignItems: 'center',
    textAlign: 'center',
    color: "#121212",
    fontSize: 20,
    textAlign:"center",
    width: 70,
    left: 40,
    top: 390,
  },
  title: {
    width: 224,
    height: 24,
    marginLeft: 10
  },
  titlePro: {
    height: 30,
    top: 400,
    left:50,
    width:300,

    backgroundColor: '#C4C4C4',
    borderRadius: 99,
  },
  flowerRow: {
    height: 24,
    flexDirection: "row",
    flex: 1
  },
});