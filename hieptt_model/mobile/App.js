import React from 'react'
import { StyleSheet} from 'react-native'
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './screens/Home'
import DetailsScreen from './screens/Detail'
import CameraScreen from './screens/Camera'


const Stack = createStackNavigator();
export default class App extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator >
          <Stack.Screen name="Home" component={HomeScreen} options={{
          headerShown: false
          // headerStyle: {
          //   display:'none',
          //   backgroundColor: 'none',
          // },
          // headerTintColor: '#fff',
          // headerTitleStyle: {
          //   fontWeight: 'bold',
          //   textAlign:'center',
          // },
        }} />
        <Stack.Screen name="Details" component={DetailsScreen} options={{
          headerStyle: {
            backgroundColor: 'transparent',
          },
          // headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign:'center',
          },
        }}/>
        <Stack.Screen name="Camera" component={CameraScreen} options={{
          headerStyle: {
            backgroundColor: 'transparent',
          },
          // headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
            textAlign:'center',
          },
        }}/>
        </Stack.Navigator>
      </NavigationContainer>
    )
  }
}